FROM ubuntu:18.04 AS builder

# Install dependencies
RUN apt-get update && apt-get install -y \
	unzip \
	curl \
	cmake \
	git \
	build-essential \
	gcc \
	g++ \
	zlib1g-dev \
	libaio-dev \
	libssl1.0-dev;

# Download Percona XtraDB tarball
RUN curl -L https://downloads.percona.com/downloads/Percona-XtraDB-Cluster-80/Percona-XtraDB-Cluster-8.0.20-11.2/binary/tarball/Percona-XtraDB-Cluster_8.0.20-11.2_Linux.x86_64.glibc2.12.tar.gz -o percona-xtradb-cluster.tar.gz

# Extract tarball
RUN tar -xf percona-xtradb-cluster.tar.gz && pwd

# Git clone pstress
RUN git clone https://github.com/Percona-QA/pstress.git && cd pstress && git clean -fd

# Build pstress
RUN cd /pstress && \
	cmake . -DPERCONACLUSTER=ON -DBASEDIR=/Percona-XtraDB-Cluster_8.0.20-11.2_Linux.x86_64.glibc2.12 && \
	make install

FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
	zlib1g-dev \
	libaio-dev \
	libssl1.0-dev;

COPY --from=builder /usr/local/bin/pstress-pxc /usr/local/bin/pstress-pxc

ENTRYPOINT ["pstress-pxc"]